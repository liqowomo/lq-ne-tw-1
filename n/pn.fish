#!/bin/fish
echo (set_color magenta) "--------"
echo ""
echo "PNPM related Instals"
echo "1. Updated PNPM "
echo (set_color yellow)" > pnpm add -g pnpm"
echo ""
echo "2. Set PNPM auto install all peers to true - "
echo (set_color yellow) "> pnpm config set auto-install-peers true"
echo (set_color magenta) "--------"
echo ""
pnpm add -g pnpm
echo (set_color green)"DONE  = pnpm add -g pnpm"
pnpm config set auto-install-peers true 
bat .npmrc 
echo (set_color green)"DONE  = pnpm config set auto-install-peers true"