<h1 align="center"><code>💮:LQ-NE-TW-1</code></h1>
<h2 align="center"><i> NextJS13 + TailwindCSS Learn 1 </i></h2>

----
1. [Huh ?](#huh-)
   1. [Next JS 13](#next-js-13)
   2. [TailWind CSS](#tailwind-css)
2. [PNPM Initial Setup](#pnpm-initial-setup)
   1. [Update PNPM](#update-pnpm)
   2. [Automatically install all peers](#automatically-install-all-peers)
3. [Usage Commands](#usage-commands)
   1. [Step1 - Install Next JS 13](#step1---install-next-js-13)
   2. [Step 2 - Install TailwindCSS](#step-2---install-tailwindcss)
   3. [Step3 - TW Setup - Config Paths](#step3---tw-setup---config-paths)
   4. [Step 4 - TW -  Add directives](#step-4---tw----add-directives)
   5. [Step 5 - Run Dev](#step-5---run-dev)
   6. [Step 6 - TW - Add Classes](#step-6---tw---add-classes)

----

# Huh ? 

1. Learning NextJS 13 and tailwind css usage 

## Next JS 13

[`https://beta.nextjs.org/docs`](https://beta.nextjs.org/docs)
- Official Beta DOCS 

## TailWind CSS 

[`https://tailwindcss.com/docs/guides/nextjs`](https://tailwindcss.com/docs/guides/nextjs)
- Official framework docs 

# PNPM Initial Setup

1. Will be using this instead everywhere 

## Update PNPM 

```sh 
pnpm add -g pnpm
```

## Automatically install all peers 

```sh
pnpm config set auto-install-peers true 
```


# Usage Commands 

## Step1 - Install Next JS 13 

- Taken form the ['manual'](https://beta.nextjs.org/docs/installation)
- Will be using PNPX
  - `PNPX` is `NPX` equivalent

```sh 
pnpx create-next-app@latest --experimental-app
```
- Note the `pnpx` at the start

## Step 2 - Install TailwindCSS

```sh 
pnpm install -D tailwindcss postcss autoprefixer
pnpx tailwindcss init -p
```
- note `pnpm` = Is being used for the install 
- note `pnpx` = Is bening used instead of `npx`

## Step3 - TW Setup - Config Paths 

File - `tailwind.config.js`

```js 
/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx}",
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}
```
- Note - `./app` = This is taking into account the new nextjs 13.2 dir structure 

## Step 4 - TW -  Add directives 

File - `globals.css` 
- Located in the `app/` 
- Remove all and add 

```js 
@tailwind base;
@tailwind components;
@tailwind utilities;
```
- This can throw some warnings, ignore 

## Step 5 - Run Dev

```sh 
pnpm run dev
```
## Step 6 - TW - Add Classes

- Default documentation talks about 'index.tsx' , but this is not deprecated and you should add this to 
  - `page.tsx` = Contents of `index.tsx` goes here 
  - `layout.tsx` = This will apply the overall styles
  - `*.css` = Either 'global' of 'pagespecific' , is where you can still add custom `css` 